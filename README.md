# "Comment mieux prendre soin de nos aînés ?" static page

URL: https://grande-consultation-aines.make.org

Gulp project to minify static page for "Comment mieux prendre soin de nos aînés ?" consultation

## Install
```shell
npm install
```

## Clean dist folder
```shell
gulp clean
```

## Build dist folder
```shell
gulp
```

## Minify CSS files
```shell
gulp styles
```

## Minify JS files
```shell
gulp scripts
```

## Minify HTML file
```shell
gulp pages
```

## Copy images files to dist
```shell
gulp images
```

## Copy webfonts files to dist
```shell
gulp webfonts
```
